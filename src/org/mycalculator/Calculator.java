package org.mycalculator;

public interface Calculator <E> {
    public E add(E e1, E e2);
    public E sub(E e1, E e2);
    public E mul(E e1, E e2);
    public E div(E e1, E e2);
}
