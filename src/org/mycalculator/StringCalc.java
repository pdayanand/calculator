package org.mycalculator;

public class StringCalc implements Calculator {
    @Override
    public Object add(Object e1, Object e2) {
        return String.format("%s + %s", (String) e1.toString(), (String) e2.toString());
    }

    @Override
    public Object sub(Object e1, Object e2) {
        return String.format("%s - %s", (String) e1.toString(), (String) e2.toString());
    }

    @Override
    public Object mul(Object e1, Object e2) {
        return String.format("%s * %s", (String) e1.toString(), (String) e2.toString());
    }

    @Override
    public Object div(Object e1, Object e2) {
        return String.format("%s / %s", (String) e1.toString(), (String) e2.toString());
    }
}
