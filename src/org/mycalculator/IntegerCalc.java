package org.mycalculator;

public class IntegerCalc implements Calculator {
    @Override
    public Object add(Object e1, Object e2) {
        return (int) e1 + (int) e2;
    }

    @Override
    public Object sub(Object e1, Object e2) {
        return (int) e1 - (int) e2;
    }

    @Override
    public Object mul(Object e1, Object e2) {
        return (int) e1 * (int) e2;
    }

    @Override
    public Object div(Object e1, Object e2) {
        return (int) e1 / (int) e2;
    }
}
